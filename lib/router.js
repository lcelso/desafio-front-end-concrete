App.Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		'show': 'show'
	},

	index: function(){
		console.log("Index route has been called..");
	},

	show: function(){
		console.log("Show route has been called..");
	},
});

new App.Router;
Backbone.history.start();