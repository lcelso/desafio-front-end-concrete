# README #

### DESAFIO FRONT END - CONCRETE ###

Proposta:
Implementar uma aplicação completamente client-side, que consulte a API do Dribbble e mostre os shots mais populares. Esta aplicação deve funcionar nos navegadores mais recentes do mercado.

### O PRODUTO ###
* Foi utilizado o [Backbone.js](http://backbonejs.org) como framework, com [Underscore.js](http://underscorejs.org).

* Utilizado o [LESS CSS](http://lesscss.org), utilizando alguns mixis, variaveis, funções, operações e utilizando algumas regras do [SMACCSS](http://smacss.org).

* Backbone, crie a seguinte estrutura:

** App;
** Collections;
** Models;
** Views;
** Router; (Este item eu apenas inicie)

### O PLUS ###
Fui um pouco além do que havia sido solicitado, criei uma modal onde é exibida a foto em maior escala do projeto do user, com a imagem de profile dele, nome, bio do user, e description do projeto.